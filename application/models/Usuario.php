<?php
    class Usuario extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("usuario",$datos);
      }

    public function obtenerTodos(){
      $this->db->order_by("apellido_usu","asc");
      $this->db->where("perfil_usu","PACIENTE");
      $listado=$this->db->get("usuario");
      if($listado->num_rows()>0){
        return $listado;
      }else{
        return false;
      }
    }

    public function obtenerPorId($id_usu){
      $this->db->where("id_usu",$id_usu);
      $this->db->where("path_usu",$path_usu);
      $usuario=$this->db->get("usuario");
      if($usuario->num_rows()>0){
        return $usuario->row();
      }else{
        return false;
      }
    }

    public function eliminar($id_usu){
        $this->db->where("id_usu",$id_usu);
        return $this->db->delete("usuario");
    }

    public function actualizar($data, $id_usu){
        $this->db->where("id_usu",$id_usu);
        return $this->db->update("usuario",$data);
    }


      //funcion para sacar el detalle de un usuario
      public function consultarPorId($id_usu){
        $this->db->where("id_usu",$id_usu);
        //  $this->db->where("path_usu",$path_usu);
        $usuario=$this->db->get("usuario");
        if($usuario->num_rows()>0){
          return $usuario->row();//cuando SI hay usuarios
        }else{
          return false;//cuando NO hay usuarios
        }
      }
      //funcion para consultar todos lo usuarios
      public function consultarTodos(){
          $listadoUsuarios=$this->db->get("usuario");

          if($listadoUsuarios->num_rows()>0){
            return $listadoUsuarios;//cuando SI hay usuarios
          }else{
            return false;//cuando NO hay usuarios
          }
      }

   }//cierre de la clase
