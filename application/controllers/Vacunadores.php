<?php
      class Vacunadores extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("vacunador");

        }

        public function index(){
          $data["listadoVacunadores"]=$this->vacunador->consultarTodos();
          $this->load->view("header");
          $this->load->view("vacunadores/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("vacunadores/nuevo");
          $this->load->view("footer");
        }

        public function guardarVacunador(){
            $datosNuevoVacunador=array(
                "nombre_vacu"=>$this->input->post("nombre_vacu"),
                "apellido_vacu"=>$this->input->post("apellido_vacu"),
                "estado_vacu"=>$this->input->post("estado_vacu")
            );
            if($this->vacunador->insertar($datosNuevoVacunador)){

            }else{
              echo "Error";
            }
            redirect("vacunadores/index");
        }

        public function procesarEliminacion($id_vacu){
          if ($this->vacunador->eliminar($id_vacu)) {

          }else {
              echo "Error";
          }
          redirect("vacunadores/index");

        }

        public function editar($id_vacu){
          $data["vacunador"]=$this->vacunador->consultarPorId($id_vacu);
          $this->load->view("header");
          $this->load->view("vacunadores/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_vacu=$this->input->post("id_vacu");
          $datosVacunadorEditado=array(
            "nombre_vacu"=>$this->input->post("nombre_vacu"),
            "apellido_vacu"=>$this->input->post("apellido_vacu"),
            "estado_vacu"=>$this->input->post("estado_vacu")
        );
          if($this->vacunador->actualizar($id_vacu,$datosVacunadorEditado)){

          }else{
              echo "Error";
          }
          redirect("vacunadores/index");
        }

        }
?>
